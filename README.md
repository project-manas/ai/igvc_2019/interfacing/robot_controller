# Robot Controller

ROS package to convert cmd_vel commands to left and right wheel velocities.

### Usage
Set the following parameters in `robot_controller.launch`:
  - `cmd_vel_topic (defualt: "/cmd_vel")`: Topic where cmd_vel commands are published (type: *geometry_msgs/Twist*)
  - `left_wheel_topic (default: "/left_wheel_velocity")`: Topic where left wheel commands should be published (type: *std_msgs/Float64*)
  - `left_wheel_topic (default: "/right_wheel_velocity")`: Topic where right wheel commands should be published (type: *std_msgs/Float64*)
  - `wheel_radius (default: 0.2)`: Radius of robot wheels (in meters)
  - `wheel_base (default: 0.618)`: Distance between right anf left wheels (in meters)
  - `publish_rpm (default: false)`: Publish right and left RPM values instead of velocity values

To launch the robot controller node:
```bash
roslaunch robot_controller robot_controller.launch
```
