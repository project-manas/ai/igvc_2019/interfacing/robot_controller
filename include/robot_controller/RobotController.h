//
// Created by naivehobo on 5/5/19.
//

#ifndef ROBOT_CONTROLLER_ROBOTCONTROLLER_H
#define ROBOT_CONTROLLER_ROBOTCONTROLLER_H

#include <ros/ros.h>

#include <std_msgs/Float64.h>
#include <geometry_msgs/Twist.h>

class RobotController {

 public:
  RobotController();

  void cmdVelCallback(const geometry_msgs::TwistConstPtr &cmd);

 private:
  ros::NodeHandle nh_;
  ros::NodeHandle private_nh_;

  ros::Publisher left_wheel_pub_;
  ros::Publisher right_wheel_pub_;

  ros::Subscriber cmd_vel_sub_;

  double wheel_radius_;
  double wheel_base_;
  bool publish_rpm_;

  std::string cmd_vel_topic_;
  std::string right_wheel_topic_;
  std::string left_wheel_topic_;

};

#endif //PERSON_TRACKING_PERSONTRACKER_H
