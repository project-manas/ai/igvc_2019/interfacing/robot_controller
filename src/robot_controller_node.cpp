//
// Created by naivehobo on 5/5/19.
//

#include <ros/ros.h>

#include <robot_controller/RobotController.h>


int main(int argc, char** argv) {
  ros::init(argc, argv, "robot_controller");

  auto controller = RobotController();

  ros::spin();

  return 0;
}