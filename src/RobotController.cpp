//
// Created by naivehobo on 5/5/19.
//

#include "robot_controller/RobotController.h"

#include <ros/ros.h>

#include <geometry_msgs/Twist.h>
#include <std_msgs/Float64.h>

#include <math.h>

#define RPM_TO_RAD_PER_S (2*M_PI/60.0)

RobotController::RobotController() : private_nh_("~") {
  private_nh_.param("wheel_radius", wheel_radius_, 0.2);
  private_nh_.param("wheel_base", wheel_base_, 0.618);
  private_nh_.param("publish_rpm", publish_rpm_, false);
  private_nh_.param<std::string>("cmd_vel_topic", cmd_vel_topic_, "cmd_vel");
  private_nh_.param<std::string>("left_wheel_topic", left_wheel_topic_, "left_wheel_velocity");
  private_nh_.param<std::string>("right_wheel_topic", right_wheel_topic_, "right_wheel_velocity");

  left_wheel_pub_ = nh_.advertise<std_msgs::Float64>(left_wheel_topic_, 1);
  right_wheel_pub_ = nh_.advertise<std_msgs::Float64>(right_wheel_topic_, 1);

  cmd_vel_sub_ = nh_.subscribe(cmd_vel_topic_, 1, &RobotController::cmdVelCallback, this);
}

void RobotController::cmdVelCallback(const geometry_msgs::TwistConstPtr &cmd) {
  double right = (cmd->angular.z * wheel_base_) * 0.5 + cmd->linear.x;
  double left = cmd->linear.x * 2 - right;

  if (publish_rpm_) {
    right = right / (RPM_TO_RAD_PER_S * wheel_radius_);
    left = left / (RPM_TO_RAD_PER_S * wheel_radius_);
  }

  std_msgs::Float64 msg;

  msg.data = left;
  left_wheel_pub_.publish(msg);

  msg.data = right;
  right_wheel_pub_.publish(msg);
}
